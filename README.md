# FindWork CRM PHP Client

## How to use

```php
<?php
    require __DIR__.'/vendor/autoload.php';

    use FindWork\CRM\Adapters\Streak\Client;
    use FindWork\Transporter\Adapters\GuzzleAdapter;

    // Streak is the first CRM that i've created the adapters

    $base_uri = "base uri streak v1";
    $api_key = "your api key";

    // using my own http client adapter package to handle the http client method
    // Anyone can use any other http client
    $client = new GuzzleAdapter([
        'base_uri' => $base_uri,
        'auth' => [$api_key, '']
        ]);

    $crm = new Client($client);
    $pipelines = $crm->pipelines()->getAll();
?>
```

### coming soon for other CRM

## Eventbrite Usage

```php
<?php
    require __DIR__.'/vendor/autoload.php';

    use FindWork\CRM\Adapters\Eventbrite\Client;
    use FindWork\Transporter\Adapters\GuzzleAdapter;

    // Streak is the first CRM that i've created the adapters

    $base_uri = "base uri eventbrite api v3";
    $token = "your secret eventbrite token";

    // using my own http client adapter package to handle the http client method
    // Anyone can use any other http client

    // Set token on authorization

    $headers = [
        'Authorization' => 'Bearer '.$token
    ];

    $client = new GuzzleAdapter([
        'base_uri' => $base_uri,
        'headers' => $headers
        ]);

    $eb = new Client($client);
    $org = $eb->organizations()->get();

    // Get your account organization id
    $orgid = $org['organizations'][0]['id'];

    $events = $eb->events()->getAll($orgid);
?>
```