<?php

namespace FindWork\CRM\Contracts;

abstract class AbstractCrmAdapter implements CrmAdapterInterface
{
    protected $client = false;

    public function __construct($client)
    {
        $this->client = $client;
    }
}