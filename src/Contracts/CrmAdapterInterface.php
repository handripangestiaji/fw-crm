<?php

namespace FindWork\CRM\Contracts;

interface CrmAdapterInterface
{
    public function getAll(string $key) : array;
    public function get(string $key) : array;
    public function create(string $key, array $items = []) : array;
}