<?php

namespace FindWork\CRM\Adapters\Eventbrite\Endpoints;

use FindWork\CRM\Contracts\AbstractCrmAdapter;
use Exception;

class Series extends AbstractCrmAdapter
{
    const ENDPOINT = 'series';

    public function getAll(string $seriesid = null, array $params = []) : array
    {
        if(!$seriesid)
        {
            throw new Exception('Series ID is required', 400);
        }
        
        return $this->client->get(sprintf('%s/%s', self::ENDPOINT, $seriesid), [
            'query' => $params
        ]);
    }

    public function get(string $seriesid = null) : array
    {
        if(!$seriesid)
        {
            throw new Exception('Series ID is required', 400);
        }

        return $this->client->get(sprintf('%s/%s/%s', self::ENDPOINT, $seriesid, 'events'));
    }

    public function create(string $orgid = null, array $items = []) : array
    {
        if(!$orgid)
        {
            throw new Exception('Organizations ID is required', 400);
        }

        if(empty($items))
        {
            throw new Exception('Items can not be empty', 400);
        }

        return $this->client->post(sprinft('%s/%s/%', 'organizations', $orgid, self::ENDPOINT), [
            'form_params' => $items
        ]);
    }
}