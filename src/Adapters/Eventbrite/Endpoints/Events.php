<?php

namespace FindWork\CRM\Adapters\Eventbrite\Endpoints;

use FindWork\CRM\Contracts\AbstractCrmAdapter;
use Exception;

class Events extends AbstractCrmAdapter
{
    const ENDPOINT = 'events';

    public function getAll(string $orgid = null, array $params = []) : array
    {
        if(!$orgid)
        {
            throw new Exception('Organizations ID is required', 400);
        }

        return $this->client->get(sprintf('%s/%s/%s', 'organizations', $orgid, self::ENDPOINT), [
            'query' => $params
        ]);
    }

    public function get(string $eventid = null) : array
    {
        if(!$eventid)
        {
            throw new Exception('Event ID is required', 400);
        }

        return $this->client->get(sprintf('%s/%s', self::ENDPOINT, $eventid));
    }

    public function create(string $orgid = null, array $items = []) : array
    {
        if(!$orgid)
        {
            throw new Exception('Organizations ID is required', 400);
        }

        if(empty($items))
        {
            throw new Exception('Items can not be empty', 400);
        }

        return $this->client->post(sprintf('%s/%s/%s', 'organizations', $orgid, self::ENDPOINT), [
            'form_params' => $items
        ]);
    }
    
    public function update(string $eventid = null, array $items = []) : array
    {
        if(!$eventid)
        {
            throw new Exception('Event ID is required', 400);
        }

        if(empty($items))
        {
            throw new Exception('Items can not be empty', 400);
        }

        return $this->client->post(sprintf('%s/%s', self::ENDPOINT, $eventid), [
            'form_params' => $items
        ]);
    }
}