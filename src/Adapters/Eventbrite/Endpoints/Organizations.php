<?php

namespace FindWork\CRM\Adapters\Eventbrite\Endpoints;

use FindWork\CRM\Contracts\AbstractCrmAdapter;
use Exception;

class Organizations extends AbstractCrmAdapter
{
    const ENDPOINT = 'organizations';
    
    public function getAll(string $userid = null) : array
    {

    }

    public function get(string $userid = null) : array
    {
        return $this->client->get(sprintf('%s/%s/%s', 'users', $userid ? $userid : 'me', self::ENDPOINT));
    }

    public function create(string $userid = null, array $items = []) : array
    {

    }
}