<?php

namespace FindWork\CRM\Adapters\Eventbrite;

use FindWork\CRM\Adapters\Eventbrite\Endpoints\Organizations;
use FindWork\CRM\Adapters\Eventbrite\Endpoints\Events;
use FindWork\CRM\Adapters\Eventbrite\Endpoints\Series;

class Client
{
    protected $client;
    
    public function __construct($client)
    {
        $this->client = $client;
    }

    public function organizations()
    {
        return new Organizations($this->client);
    }

    public function events()
    {
        return new Events($this->client);
    }

    public function series()
    {
        return new Series($this->client);
    }
}