<?php

namespace FindWork\CRM\Adapters\Streak;

use FindWork\CRM\Adapters\Streak\Endpoints\Boxes;
use FindWork\CRM\Adapters\Streak\Endpoints\Pipelines;
use FindWork\CRM\Adapters\Streak\Endpoints\Users;
use FindWork\CRM\Adapters\Streak\Endpoints\Teams;
use FindWork\CRM\Adapters\Streak\Endpoints\Contacts;

class Client {
    protected $client;
    
    public function __construct($client)
    {
        $this->client = $client;
    }

    public function boxes()
    {
        return new Boxes($this->client);
    }

    public function pipelines()
    {
        return new Pipelines($this->client);
    }

    public function users()
    {
        return new Users($this->client);
    }

    public function teams()
    {
        return new Teams($this->client);
    }

    public function contacts()
    {
        return new Contacts($this->client);
    }
}