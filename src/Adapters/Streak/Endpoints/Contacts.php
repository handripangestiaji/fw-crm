<?php

namespace FindWork\CRM\Adapters\Streak\Endpoints;

use FindWork\CRM\Contracts\AbstractCrmAdapter;
use Exception;

class Contacts extends AbstractCrmAdapter
{
    const API_VERSION = 'v2';
    const ENDPOINT = 'contacts';

    public function getAll(string $key = null) : array
    {
        
    }

    public function get(string $contactkey = null) : array
    {
        if(!$contactkey)
        {
            throw new Exception('Contact key is required', 400);
        }

        return $this->client->get(sprintf('%s/%s/%s', self::API_VERSION, self::ENDPOINT, $contactkey));
    }

    public function create(string $teamkey = null, array $items = []) : array
    {
        if(!$teamkey)
        {
            throw new Exception('Team key is required', 400);
        }

        if(empty($items))
        {
            throw new Exception('Contacts can not be empty', 400);
        }

        if(!array_key_exists('emailAddresses', $items)) 
        {
            throw new Exception('Contacts should contain email addresses', 400);
        } 
            
        if(!is_array($items['emailAddresses']))
        {
            throw new Exception('Email addresses should be an array', 400);
        }

        if(empty($items['emailAddresses']))
        {
            throw new Exception('Email addresses can not be empty', 400);
        }

        return $this->client->post(sprintf('%s/teams/%s/%s', self::API_VERSION, $teamkey, self::ENDPOINT), [
            'headers' => ['Content-type' => 'application/json'],
            'body' => json_encode($items)
        ]);
    }
}