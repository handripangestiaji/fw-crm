<?php

namespace FindWork\CRM\Adapters\Streak\Endpoints;

use FindWork\CRM\Contracts\AbstractCrmAdapter;
use Exception;

class Teams extends AbstractCrmAdapter
{
    const API_VERSION = 'v2';
    const ENDPOINT = 'teams';

    public function getAll(string $teamkey = null) : array
    {
        return $this->client->get(sprintf('%s/users/me/%s', self::API_VERSION, self::ENDPOINT));
    }

    public function get(string $teamkey = null) : array
    {
        if(!$teamkey)
        {
            throw new Exception('Team key is required', 400);
        }

        return $this->client->get(sprintf('%s/%s/%s', self::API_VERSION, self::ENDPOINT, $teamkey));
    }

    public function create(string $teamkey = null, array $items = []) : array
    {

    }
}