<?php

namespace FindWork\CRM\Adapters\Streak\Endpoints;

use FindWork\CRM\Contracts\AbstractCrmAdapter;
use Exception;

class Boxes extends AbstractCrmAdapter
{
    const API_VERSION = 'v1';
    const ENDPOINT = 'boxes';

    public function getAll(string $pipelinekey = null) : array
    {
        if(!$pipelinekey)
        {
            throw new Exception('Pipeline is required', 400);
        }

        return $this->client->get(sprintf('%s/pipelines/%s/%s', self::API_VERSION, $pipelinekey, self::ENDPOINT));
    }

    public function get(string $boxkey = null) : array
    {
        if(!$boxkey)
        {
            throw new Exception('Box key is required', 400);
        }

        return $this->client->get(sprintf('%s/%s/%s', self::API_VERSION, $boxkey, self::ENDPOINT));
    }

    public function create(string $pipelinekey = null, array $items = []) : array
    {
        if(!$pipelinekey)
        {
            throw new Exception('Pipeline key is required', 400);
        }

        if(empty($items))
        {
            throw new Exception('Please at least put name of box', 400);
        }

        return $this->client->put(sprintf('%s/pipelines/%s/%s', self::API_VERSION, $pipelinekey, self::ENDPOINT), [
            'form_params' => $items
        ]);
    }

    public function addContacts(string $boxkey, array $items = []) : array
    {
        if(!$boxkey)
        {
            throw new Exception('Box key is required', 400);
        }

        if(empty($items))
        {
            throw new Exception('Items can not be empty', 400);
        }

        if(!array_key_exists('contacts', $items))
        {
            throw new Exception('Invalid key in array', 400);
        }

        return $this->client->post(sprintf('%s/%s/%s', self::API_VERSION, self::ENDPOINT, $boxkey), [
            'headers' => ['Content-type' => 'application/json'],
            'body' => json_encode($items)
        ]);
    }
}