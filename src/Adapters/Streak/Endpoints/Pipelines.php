<?php

namespace FindWork\CRM\Adapters\Streak\Endpoints;

use FindWork\CRM\Contracts\AbstractCrmAdapter;
use Exception;

class Pipelines extends AbstractCrmAdapter
{
    const API_VERSION = 'v1';
    const ENDPOINT = 'pipelines';

    public function getAll(string $pipelinekey = null) : array
    {
        return $this->client->get(sprintf('%s/%s', self::API_VERSION, self::ENDPOINT));
    }

    public function get(string $pipelinekey = null) : array
    {
        if(!$pipelinekey)
        {
            throw new Exception('Pipeline key is required', 400);
        }

        return $this->client->get(sprintf('%s/%s/%s', self::API_VERSION, self::ENDPOINT, $pipelinekey));
    }

    public function create(string $pipelinekey = null, array $items = []) : array
    {
        if(empty($items))
        {
            throw new Exception('Please at least put name of pipeline', 400);
        }

        return $this->client->put(sprintf('%s/%s', self::API_VERSION, self::ENDPOINT), [
            'form_params' => $items
        ]);
    }
}
