<?php

namespace FindWork\CRM\Adapters\Streak\Endpoints;

use FindWork\CRM\Contracts\AbstractCrmAdapter;
use Exception;

class Users extends AbstractCrmAdapter
{
    const API_VERSION = 'v1';
    const ENDPOINT = 'users';

    /**
     * Method to get a current active user
     */
    public function getAll(string $userkey = null) : array
    {
        return $this->client->get(sprintf('%s/%s/me', self::API_VERSION, self::ENDPOINT));
    }

    public function get(string $userkey = null) : array 
    {
        if(!$userkey)
        {
            throw new Exception('User key is required', 400);
        }

        return $this->client->get(sprintf('%s/%s/%s', self::API_VERSION, self::ENDPOINT, $userkey));
    }

    public function create(string $userkey = null, array $items = []) : array 
    {
        // THERE'S NO API FOR CREATE USER
    }
}