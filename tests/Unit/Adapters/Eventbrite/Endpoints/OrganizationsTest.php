<?php

namespace FindWork\CRM\Test\Unit\Adapters\Eventbrite\Endpoints;

use PHPUnit\Framework\TestCase;
use FindWork\CRM\Adapters\Eventbrite\Client;
use Exception;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use FindWork\Transporter\Adapters\GuzzleAdapter;

class OrganizationsTest extends TestCase
{
    public function testGetShouldReturnArray()
    {
        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $eventbrite = new Client($client);

        $result = $eventbrite->organizations()->get();

        $this->assertTrue(is_array($result));
    }
}