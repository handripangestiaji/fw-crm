<?php

namespace FindWork\CRM\Test\Unit\Adapters\Eventbrite\Endpoints;

use PHPUnit\Framework\TestCase;
use FindWork\CRM\Adapters\Eventbrite\Client;
use Exception;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use FindWork\Transporter\Adapters\GuzzleAdapter;

class EventsTest extends TestCase
{
    public function testGetAllShouldThrowExceptionIfOrgIdIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Organizations ID is required');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $orgid = '';
        $eventbrite = new Client($client);

        $eventbrite->events()->getAll($orgid);
    }

    public function testGetAllShouldReturnArray()
    {
        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $orgid = 'id';
        $params = [
            'order_by' => 'created_desc',
            'status' => 'live'
        ];
        $eventbrite = new Client($client);

        $result = $eventbrite->events()->getAll($orgid, $params);

        $this->assertTrue(is_array($result));
    }

    public function testGetShouldThrowExceptionIfEventIdIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Event ID is required');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $eventid = '';
        $eventbrite = new Client($client);

        $eventbrite->events()->get($eventid);
    }

    public function testGetShouldReturnArray()
    {
        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $eventid = 'id';
        $eventbrite = new Client($client);

        $result = $eventbrite->events()->get($eventid);

        $this->assertTrue(is_array($result));
    }

    public function testCreateShouldThrowExceptionIfOrgIdIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Organizations ID is required');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $orgid = '';
        $eventbrite = new Client($client);

        $eventbrite->events()->create($orgid);
    }

    public function testCreateShouldThrowExceptionIfItemsIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Items can not be empty');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $orgid = 'orgid';
        $items = [];
        $eventbrite = new Client($client);

        $eventbrite->events()->create($orgid, $items);
    }

    public function testCreateShouldReturnArray()
    {
        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $orgid = 'id';
        $items = [
            'title' => 'My test event',
            'description' => 'testing event creation, remember not to set the privacy or visibility of test events to "public".',
            'start_date' => date('Y-m-d H:i:s', time() + (7 * 24 * 60 * 60)),
            'end_date' => date('Y-m-d H:i:s', time() + (7 * 24 * 60 * 60) + (2 * 60 * 60) )
        ];

        $eventbrite = new Client($client);

        $result = $eventbrite->events()->create($orgid, $items);

        $this->assertTrue(is_array($result));
    }

    public function testUpdateShouldThrowExceptionIfEventIdIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Event ID is required');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $eventid = '';
        $eventbrite = new Client($client);

        $eventbrite->events()->update($eventid);
    }

    public function testUpdateShouldThrowExceptionIfItemsIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Items can not be empty');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $eventid = 'eventid';
        $items = [];
        $eventbrite = new Client($client);

        $eventbrite->events()->update($eventid, $items);
    }
}