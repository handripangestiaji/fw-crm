<?php

namespace FindWork\CRM\Test\Unit\Adapters\Eventbrite\Endpoints;

use PHPUnit\Framework\TestCase;
use FindWork\CRM\Adapters\Eventbrite\Client;
use Exception;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use FindWork\Transporter\Adapters\GuzzleAdapter;

class SeriesTest extends TestCase
{
    public function testGetAllShouldThrowExceptionIfSeriesIdIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Series ID is required');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $seriesid = '';
        $eventbrite = new Client($client);

        $eventbrite->series()->getAll($seriesid);
    }

    public function testGetAllShouldReturnArray()
    {
        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $seriesid = 'id';
        $params = [
            'time_filter' => 'all',
            'order_by' => 'created_desc'
        ];

        $eventbrite = new Client($client);

        $result = $eventbrite->series()->getAll($seriesid, $params);

        $this->assertTrue(is_array($result));
    }

    public function testGetShouldThrowExceptionIfSeriesIdIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Series ID is required');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $seriesid = '';
        $eventbrite = new Client($client);

        $eventbrite->series()->get($seriesid);
    }

    public function testGetShouldReturnArray()
    {
        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $seriesid = 'id';
        $eventbrite = new Client($client);

        $result = $eventbrite->series()->get($seriesid);

        $this->assertTrue(is_array($result));
    }

    public function testCreateShouldThrowExceptionIfOrgIdIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Organizations ID is required');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $orgid = '';
        $eventbrite = new Client($client);

        $eventbrite->series()->create($orgid);
    }
}