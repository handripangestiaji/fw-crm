<?php

namespace FindWork\CRM\Test\Unit\Adapters\Endpoints;

use PHPUnit\Framework\TestCase;
use Exception;
use FindWork\Transporter\Adapters\GuzzleAdapter;
use FindWork\Transporter\Contracts\HttpTransportInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use FindWork\CRM\Adapters\Streak\Client;

class BoxesTest extends TestCase
{
    public function testGetAllShouldThrowExceptionIfPipelineKeyIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Pipeline is required');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $streak = new Client($client);

        $streak->boxes()->getAll();
    }

    public function testGetAllShouldReturnArray()
    {
        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $streak = new Client($client);

        $result = $streak->boxes()->getAll('pipelinekey');

        $this->assertTrue(is_array($result));
    }

    public function testGetShouldThrowExceptionIfBoxKeyIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Box key is required');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $streak = new Client($client);

        $streak->boxes()->get();
    }

    public function testGetShouldReturnArray()
    {
        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $streak = new Client($client);

        $result = $streak->boxes()->get('boxkey');

        $this->assertTrue(is_array($result));
    }

    public function testCreateShouldThrowExceptionIfPipelineKeyIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Pipeline key is required');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $pipelinekey = '';
        $items = ['name' => 'test'];
        $streak = new Client($client);

        $streak->boxes()->create($pipelinekey, $items);
    }

    public function testCreateShouldThrowExceptionIfItemsIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Please at least put name of box');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $pipelinekey = 'testkey';
        $items = [];
        $streak = new Client($client);

        $streak->boxes()->create($pipelinekey, $items);
    }

    public function testCreateShouldReturnArray()
    {
        $mock = new MockHandler([new Response(200, [], '[{"result": "Failed to create a box"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $pipelinekey = 'testkey';
        $items = ['name' => 'test'];
        $streak = new Client($client);

        $result = $streak->boxes()->create($pipelinekey, $items);

        $this->assertTrue(is_array($result));
    }

    public function testAddContactsShouldThrowExceptionIfBoxKeyIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Box key is required');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $boxkey = '';
        $items = [
            'contacts' => [
                'key' => 'contact-key'
            ]
        ];

        $streak = new Client($client);

        $streak->boxes()->addContacts($boxkey, $items);
    }

    public function testAddContactsShouldThrowExceptionIfItemsIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Items can not be empty');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $boxkey = 'testkey';
        $items = [];

        $streak = new Client($client);

        $streak->boxes()->addContacts($boxkey, $items);
    }

    public function testAddContactsShouldThrowExceptionIfInvalidKeyInItems()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Invalid key in array');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $boxkey = 'testkey';
        $items = [
            'kontak' => [
                'key' => 'contact-key'
            ]
        ];

        $streak = new Client($client);

        $streak->boxes()->addContacts($boxkey, $items);
    }
}