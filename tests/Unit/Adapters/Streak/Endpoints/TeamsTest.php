<?php

namespace FindWork\CRM\Test\Unit\Adapters\Endpoints;

use PHPUnit\Framework\TestCase;
use Exception;
use FindWork\Transporter\Adapters\GuzzleAdapter;
use FindWork\Transporter\Contracts\HttpTransportInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use FindWork\CRM\Adapters\Streak\Client;

class TeamsTest extends TestCase
{
    public function testGetAllShouldReturnArray()
    {
        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $streak = new Client($client);

        $result = $streak->teams()->getAll();

        $this->assertTrue(is_array($result));
    }

    public function testGetShouldThrowExceptionIfTeamKeyIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Team key is required');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $teamkey = '';
        $streak = new Client($client);

        $streak->teams()->get($teamkey);
    }

    public function testGetShouldReturnArray()
    {
        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $teamkey = 'testkey';
        $streak = new Client($client);

        $result = $streak->teams()->get($teamkey);

        $this->assertTrue(is_array($result));
    }
}

