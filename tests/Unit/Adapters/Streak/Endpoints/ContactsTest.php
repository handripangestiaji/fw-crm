<?php

namespace FindWork\CRM\Test\Unit\Adapters\Endpoints;

use PHPUnit\Framework\TestCase;
use Exception;
use FindWork\Transporter\Adapters\GuzzleAdapter;
use FindWork\Transporter\Contracts\HttpTransportInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use FindWork\CRM\Adapters\Streak\Client;

class ContactsTest extends TestCase
{
    public function testGetShouldThrowExceptionIfContactKeyIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Contact key is required');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $contactkey = '';
        $streak = new Client($client);

        $streak->contacts()->get($contactkey);
    }

    public function testGetShouldReturnArray()
    {
        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $contactkey = 'testkey';
        $streak = new Client($client);

        $result = $streak->contacts()->get($contactkey);

        $this->assertTrue(is_array($result));
    }

    public function testCreateShouldThrowExceptionIfTeamKeyIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Team key is required');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $teamkey = '';
        $items = [
            'givenName' => 'Handri',
            'lastName' => 'Pangestiaji',
            'emailAddresses' => ['handri.pangestiaji@gmail.com']
        ];

        $streak = new Client($client);
        $streak->contacts()->create($teamkey, $items);
    }

    public function testCreateShouldThrowExceptionIfItemsIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Contacts can not be empty');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $teamkey = 'testkey';
        $items = [];

        $streak = new Client($client);
        $streak->contacts()->create($teamkey, $items);
    }

    public function testCreateShouldThrowExceptionIfEmailAddressesIsNotSet()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Contacts should contain email addresses');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $teamkey = 'testkey';
        $items = [
            'givenName' => 'Handri',
            'lastName' => 'Pangestiaji'
        ];

        $streak = new Client($client);
        $streak->contacts()->create($teamkey, $items);
    }

    public function testCreateShouldThrowExceptionIfEmailAddressesIsNotArray()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Email addresses should be an array');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $teamkey = 'testkey';
        $items = [
            'givenName' => 'Handri',
            'lastName' => 'Pangestiaji',
            'emailAddresses' => ''
        ];

        $streak = new Client($client);
        $streak->contacts()->create($teamkey, $items);
    }

    public function testCreateShouldThrowExceptionIfEmailAddressesIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Email addresses can not be empty');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $teamkey = 'testkey';
        $items = [
            'givenName' => 'Handri',
            'lastName' => 'Pangestiaji',
            'emailAddresses' => []
        ];

        $streak = new Client($client);
        $streak->contacts()->create($teamkey, $items);
    }

    public function testCreateShouldReturnArray()
    {
        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $teamkey = 'testkey';
        $items = [
            'givenName' => 'Handri',
            'lastName' => 'Pangestiaji',
            'emailAddresses' => ['handri.pangestiaji@gmail.com']
        ];

        $streak = new Client($client);
        $result = $streak->contacts()->create($teamkey, $items);

        $this->assertTrue(is_array($result));
    }
}