<?php

namespace FindWork\CRM\Test\Unit\Adapters\Endpoints;

use PHPUnit\Framework\TestCase;
use Exception;
use FindWork\Transporter\Adapters\GuzzleAdapter;
use FindWork\Transporter\Contracts\HttpTransportInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use FindWork\CRM\Adapters\Streak\Client;

class PipelinesTest extends TestCase
{
    public function testGetAllShouldReturnArray()
    {
        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $streak = new Client($client);

        $result = $streak->pipelines()->getAll();

        $this->assertTrue(is_array($result));
    }

    public function testGetShouldThrowExceptionIfPipelineKeyIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Pipeline key is required');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $pipelinekey = '';
        $streak = new Client($client);

        $streak->pipelines()->get($pipelinekey);
    }

    public function testGetShouldReturnArray()
    {
        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $pipelinekey = 'testkey';
        $streak = new Client($client);

        $result = $streak->pipelines()->get($pipelinekey);

        $this->assertTrue(is_array($result));
    }

    public function testCreateShouldThrowExceptionIfItemsIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Please at least put name of pipeline');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $items = [];
        $streak = new Client($client);

        $streak->pipelines()->create(null, $items);
    }

    public function testCreateShouldReturnArray()
    {
        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $items = [
            'name' => 'test'
        ];
        $streak = new Client($client);

        $result = $streak->pipelines()->create(null, $items);

        $this->assertTrue(is_array($result));
    }
}