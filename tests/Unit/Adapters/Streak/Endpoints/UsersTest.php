<?php

namespace FindWork\CRM\Test\Unit\Adapters\Endpoints;

use PHPUnit\Framework\TestCase;
use Exception;
use FindWork\Transporter\Adapters\GuzzleAdapter;
use FindWork\Transporter\Contracts\HttpTransportInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use FindWork\CRM\Adapters\Streak\Client;

class UsersTest extends TestCase
{
    public function testGetAllShouldReturnArray()
    {
        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $streak = new Client($client);

        $result = $streak->users()->getAll();

        $this->assertTrue(is_array($result));
    }

    public function testGetShouldThrowExceptionIfUserKeyIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('User key is required');
        $this->expectExceptionCode(400);

        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['base_uri' => 'http://api.test.xyz/', 'handler' => $handler]);

        $userkey = '';
        $streak = new Client($client);

        $streak->users()->get($userkey);
    }

    public function testGetShouldReturnArray()
    {
        $mock = new MockHandler([new Response(200, [], '[{"result": "test"}]')]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleAdapter(['handler' => $handler]);

        $userkey = 'testkey';
        $streak = new Client($client);

        $result = $streak->users()->get($userkey);

        $this->assertTrue(is_array($result));
    }
}